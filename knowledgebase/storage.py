# Teken from https://medium.com/@mateusz-jasinski/using-minio-with-django-for-local-development-80bda22927

from storages.backends.s3boto3 import S3Boto3Storage
from django.conf import settings

# class StaticS3Boto3Storage(S3Boto3Storage):
#    location = settings.STATICFILES_LOCATION
#
#    def __init__(self, *args, **kwargs):
#        if settings.MINIO_ACCESS_URL:
#            self.secure_urls = False
#            self.custom_domain = settings.MINIO_ACCESS_URL
#        super(StaticS3Boto3Storage, self).__init__(*args, **kwargs)


class S3MediaStorage(S3Boto3Storage):
    def __init__(self, *args, **kwargs):
        if settings.S3_PUBLIC_ACCESS_DOMAIN:
            # For parameters see https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html
            if settings.DEBUG:
                self.use_ssl = False
                self.secure_urls = False  # TODO Old version
            # Setting custom_domin removes query string? But see S3Storage.url()
            self.custom_domain = settings.S3_PUBLIC_ACCESS_DOMAIN
        super().__init__(*args, **kwargs)