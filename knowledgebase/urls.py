from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from knowledgebase.models import Document
from knowledgebase import views, filters
from django_filters.views import FilterView
from knowledgebase.admin import documents_admin_site

documents_view = views.DocumentsView.as_view(
    queryset=Document.objects.order_by("-title")[:5],  # :5 limits the results to the five most recent
    context_object_name="document_list",
    template_name="knowledgebase/home.html",
)

# -------------- Taken from https://www.django-rest-framework.org/tutorial/quickstart/

from django.urls import include, path
from rest_framework import routers
from knowledgebase import views
from rest_framework.urlpatterns import format_suffix_patterns

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'actor_categories', views.ActorCategoryViewSet)
router.register(r'actors', views.ActorViewSet)
router.register(r'product_categories', views.ProductCategoryViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'location_categories', views.LocationCategoryViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'impacts', views.ImpactViewSet)
router.register(r'activities', views.ActivityViewSet)
router.register(r'topics', views.TopicViewSet)
router.register(r'documents', views.DocumentViewSet)
router.register(r'evidences', views.EvidenceViewSet)

urlpatterns = [
    path("", documents_view, name="home"),
    path("hello/<name>", views.hello, name="hello"),
    path("about/", views.about, name="about"),
    path("contact/", views.contact, name="contact"),
    path("document/", views.document, name="document"),
    path("documents_csv/", views.documents_csv, name="documents_csv"),
    # More easy way: path("search/", FilterView.as_view(filterset_class=filters.DocumentFilter), name='search'),
    path("document_search/", views.documents_search, name='document_search'),
    path('api/document_search/', views.DocumentAPISearch.as_view(), name="api_document_search"),
    path("evidence_search/", views.evidences_search, name='evidence_search'),
    path('api/evidence_search/', views.EvidenceAPISearch.as_view(), name="api_evidence_search"),
    path("grouped_evidence_search/", views.grouped_evidences_search, name='grouped_evidence_search'),
    path('api/grouped_evidence_search/', views.GroupedEvidenceAPISearch.as_view(), name="api_grouped_evidence_search"),
    path("api/", include(router.urls)),
]

urlpatterns += staticfiles_urlpatterns()