# Generated by Django 4.2.6 on 2023-11-12 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    def data_migration_relevance(apps, schema_editor):
        Evidence = apps.get_model("knowledgebase", "Evidence")
        Evidence.objects.filter(relevance='2').update(relevance2=2)
        Evidence.objects.filter(relevance='5').update(relevance2=5)
        Evidence.objects.filter(relevance='8').update(relevance2=8)
        
    dependencies = [
        ('knowledgebase', '0117_evidence_relevance2'),
    ]

    operations = [
        migrations.RunPython(data_migration_relevance)
    ]
