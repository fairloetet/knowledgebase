# Generated by Django 4.2.6 on 2023-11-25 17:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0121_alter_actorcategory_name_alter_locationcategory_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='assocs',
            field=models.ManyToManyField(blank=True, to='knowledgebase.actor', verbose_name='Association(s)'),
        ),
    ]
