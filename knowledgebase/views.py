from django.utils.timezone import datetime
from django.views.generic import ListView
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User, Group
from knowledgebase.models import Document, Activity, Actor, Evidence, Impact, Location, Product, Topic,\
                                 ActorCategory, ProductCategory, LocationCategory
from knowledgebase.filters import SimpleGroupedEvidenceFilter, DeepGroupedEvidenceFilter, DocumentFilter
from knowledgebase.forms import DocumentForm
from knowledgebase.serializers import ActorSerializer, ImpactSerializer, ProductSerializer, ActivitySerializer,\
                                      DocumentSerializer, LocationSerializer, UserSerializer, GroupSerializer,\
                                      FlatEvidenceSerializer, FlatTopicSerializer,\
                                      ActorCategorySerializer, ProductCategorySerializer, LocationCategorySerializer,\
                                      DeepGroupedEvidenceSerializer, SimpleGroupedEvidenceSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework_api_key.permissions import HasAPIKey

# -------------------------- REST Framework


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]


class ActorCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows actor categories to be viewed or edited.
    """
    queryset = ActorCategory.objects.all()
    serializer_class = ActorCategorySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ActorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows actors to be viewed or edited.
    """
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ProductCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows product categories to be viewed or edited.
    """
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows products to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class LocationCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows location categories to be viewed or edited.
    """
    queryset = LocationCategory.objects.all()
    serializer_class = LocationCategorySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class LocationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows locations to be viewed or edited.
    """
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ImpactViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows impacts to be viewed or edited.
    """
    queryset = Impact.objects.all()
    serializer_class = ImpactSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ActivityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows activities to be viewed or edited.
    """
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class TopicViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows topics to be viewed or edited.
    """
    queryset = Topic.objects.all()
    serializer_class = FlatTopicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class DocumentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows documents to be viewed or edited.
    """
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class EvidenceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows evidences to be viewed or edited.
    """
    queryset = Evidence.objects.all()
    serializer_class = FlatEvidenceSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

# ---------------------------------------


class DocumentAPISearch(generics.ListAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    filter_backends = [DjangoFilterBackend]  # From django-filters lib
    #filterset_fields = ['topics__products__pk', 'topics__locations__pk']
    filterset_class = DocumentFilter
    permission_classes = [IsAuthenticated | HasAPIKey]


class EvidenceAPISearch(generics.ListAPIView):
    queryset = Evidence.objects.all()
    serializer_class = SimpleGroupedEvidenceSerializer
    filter_backends = [DjangoFilterBackend]  # From django-filters lib
    #filterset_fields = ['topics__products__pk', 'topics__locations__pk']
    filterset_class = SimpleGroupedEvidenceFilter
    permission_classes = [IsAuthenticated | HasAPIKey]


class GroupedEvidenceAPISearch(generics.ListAPIView):
    queryset = Evidence.objects.all()
    serializer_class = DeepGroupedEvidenceSerializer
    filter_backends = [DjangoFilterBackend]  # From django-filters lib
    #filterset_fields = ['topics__products__pk', 'topics__locations__pk']
    filterset_class = DeepGroupedEvidenceFilter
    permission_classes = [IsAuthenticated | HasAPIKey]


# --------------------- Django Filter Views


def evidences_search(request):
    # request.GET contains {'location': 2} for http://127.0.0.1:8000/evidence_search/?product=&location=2
    evidence_filter = SimpleGroupedEvidenceFilter(request.GET, queryset=Evidence.objects.all())
    return render(request, 'knowledgebase/evidence_filter.html', {'filter': evidence_filter})


def grouped_evidences_search(request):
    # request.GET contains {'location': 2} for http://127.0.0.1:8000/evidence_search/?product=&location=2
    grouped_evidence_filter = DeepGroupedEvidenceFilter(request.GET, queryset=Evidence.objects.all())
    return render(request, 'knowledgebase/grouped_evidence_filter.html', {'filter': grouped_evidence_filter})


def documents_search(request):
    # request.GET contains {'location': 2} for http://127.0.0.1:8000/evidence_search/?product=&location=2
    document_filter = DocumentFilter(request.GET, queryset=Document.objects.all())
    return render(request, 'knowledgebase/document_filter.html', {'filter': document_filter})


# --------------------- Experiments based on VSCode Django integration example

class DocumentsView(ListView):
    """Renders the home page, with a list of all messages."""
    model = Document

    def get_context_data(self, **kwargs):
        context = super(DocumentsView, self).get_context_data(**kwargs)
        return context

def home(request):
    return render(request, "knowledgebase/home.html")

def about(request):
    return render(request, "knowledgebase/about.html")

def contact(request):
    return render(request, "knowledgebase/contact.html")

def hello(request, name):
    return render(
        request,
        'knowledgebase/hello.html',
        {
            'name': name,
            'date': datetime.now()
        }
    )
    
def document(request):
    form = DocumentForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            message = form.save(commit=False)
            message.log_date = datetime.now()
            message.save()
            return redirect("home")
    return render(request, "knowledgebase/document.html", {"form": form})

def documents_csv(request):
    import csv
    response = HttpResponse(
        content_type="text/csv",
        headers={"Content-Disposition": 'attachment; filename="somefilename.csv"'},
    )
    writer = csv.writer(response)
    writer.writerow(["title", "subtitle"])
    for document in Document.objects.all():
        writer.writerow([document.title, document.subtitle])
    return response
