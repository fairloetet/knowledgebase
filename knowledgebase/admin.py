from django import forms
from django.contrib import admin
from knowledgebase.models import Activity, Actor, Impact, Topic, Evidence, Document, Location, Product,\
                                 ActorCategory, ProductCategory, LocationCategory

class DocumentsAdminSite(admin.AdminSite):
    site_header = "Data"
    site_title = "Data"
    index_title = "Data"

documents_admin_site = DocumentsAdminSite(name='documents-admin')


# Mostly taken from https://stackoverflow.com/questions/66530733/
class ActorActorInline(admin.TabularInline):
    model = Actor.memberships.through
    fk_name = "to_actor"
    extra = 0
    fields = ['from_actor']
    verbose_name = "Membership"
    verbose_name_plural = "Memberships"


class ActorAdmin(admin.ModelAdmin):
    save_on_top = True
#    inlines = [ActorActorInline]
    search_fields = ['name', 'abbreviation', 'alternative_names']
    list_display = ['name', 'abbreviation', 'alternative_names', 'get_members', 'is_member']
    list_filter = ['category']
    fields = ['name', 'abbreviation', 'alternative_names', 'category', 'members']
    autocomplete_fields = ['members']
    list_select_related = ['category']


class ProductProductInline(admin.TabularInline):
    model = Product.memberships.through
    fk_name = "to_product"
    extra = 0
    fields = ['from_product']
    verbose_name = "Membership"
    verbose_name_plural = "Memberships"


class ProductAdmin(admin.ModelAdmin):
    save_on_top = True
#    inlines = [ProductProductInline]
    search_fields = ['name', 'alternative_names']
    list_display = ['name', 'category', 'alternative_names', 'has_members', 'is_member']
    list_filter = ['category']
    fields = ['name', 'alternative_names', 'category', 'members']
    autocomplete_fields = ['members']
    list_select_related = ['category']


class LocationLocationInline(admin.TabularInline):
    model = Location.memberships.through
    fk_name = "to_location"
    extra = 0
    fields = ['from_location']
    verbose_name = "Membership"
    verbose_name_plural = "Memberships"


class LocationAdmin(admin.ModelAdmin):
    save_on_top = True
#    inlines = [LocationLocationInline]
    search_fields = ['name', 'abbreviation', 'alternative_names']
    list_display = ['name', 'abbreviation', 'alternative_names', 'category', 'get_members', 'get_memberships']
    list_filter = ['category']
    fields = ['name', 'abbreviation', 'alternative_names', 'category', 'members']
    autocomplete_fields = ['members']
    list_select_related = ['category']
    

class ActivityActivityInline(admin.TabularInline):
    model = Activity.memberships.through
    fk_name = "to_activity"
    extra = 0
    fields = ['from_activity']
    verbose_name = "Membership"
    verbose_name_plural = "Memberships"


class ActivityAdmin(admin.ModelAdmin):
    save_on_top = True
#    inlines = [ActivityActivityInline]
    search_fields = ['name']
    list_display = ['name', 'get_members']
    fields = ['name', 'alternative_names', 'members']
    autocomplete_fields = ['members']


class ImpactImpactInline(admin.TabularInline):
    model = Impact.memberships.through
    fk_name = "to_impact"
    extra = 0
    fields = ['from_impact']
    verbose_name = "Membership"
    verbose_name_plural = "Memberships"


class ImpactAdmin(admin.ModelAdmin):
    save_on_top = True
#    inlines = [ImpactImpactInline]
    search_fields = ['name']
    list_display = ['name', 'get_members']
    fields = ['name', 'alternative_names', 'members']
    autocomplete_fields = ['members']


class DocumentEvidenceInline(admin.TabularInline):
    model = Evidence
    #show_change_link = True
    extra = 2
    fields = ['relevance', 'topic', 'reference']
    raw_id_fields = ['topic']    


class DocumentForm(forms.ModelForm):
    class Meta:
        widgets = {
            # Only overwrite the text area size of the title, which are too short by default
            'title': forms.TextInput(attrs={'size': '50'}),
            'subtitle': forms.TextInput(attrs={'size': '80'}),
        }
    
class DocumentAdmin(admin.ModelAdmin):
    form = DocumentForm
    save_on_top = True
    inlines = [DocumentEvidenceInline]
    date_hierarchy = 'release_date'
    list_display = ['title', 'get_assocs', 'release_date', 'created_at']
    list_filter = [('topics__products', admin.RelatedOnlyFieldListFilter), 
                   ('topics__locations', admin.RelatedOnlyFieldListFilter), 
                   ('topics__actors', admin.RelatedOnlyFieldListFilter), 
                   ('topics__impacts', admin.RelatedOnlyFieldListFilter), 
                   ('topics__activities', admin.RelatedOnlyFieldListFilter),
                   ('assocs', admin.RelatedOnlyFieldListFilter)]
    fieldsets = [
        (None, {"fields": ['title', 'subtitle', 'author', 'assocs', 'release_date', 'source', 'file']},),
        ("Advanced options",
            {
                "classes": ["collapse"],
                "fields": ['volume', 'abstract', 'note', 'todo'],
            },
         ),
    ]
    search_fields = ['title', 'author', 'abstract', 'subtitle', 'volume', 'topics__locations__name', 
                     'topics__actors__name', 'topics__products__name', 'topics__impacts__name', 
                     'topics__activities__name', 'assocs__name']
    autocomplete_fields = ['assocs']
    ordering = ['-created_at']
    view_on_site = True
    list_per_page = 50
    show_full_result_count = False


class EvidenceAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ['document_title', 'relevance', 'document_date', 'get_topic_products', 'get_topic_locations',
                    'get_topic_actors', 'get_topic_impacts', 'get_topic_activities']
    list_filter = [('topic__products', admin.RelatedOnlyFieldListFilter),
                   ('topic__locations', admin.RelatedOnlyFieldListFilter),
                   ('topic__actors', admin.RelatedOnlyFieldListFilter),
                   ('topic__impacts', admin.RelatedOnlyFieldListFilter),
                   ('topic__activities', admin.RelatedOnlyFieldListFilter)]
    list_display_links = list_display
    list_select_related = ['topic', 'document']

    @admin.display(description="Products")
    def get_topic_products(self, evidence: Evidence):
        return [product.name for product in evidence.topic.products.all()] or "-"

    @admin.display(description="Locations")
    def get_topic_locations(self, evidence: Evidence):
        return [location.name for location in evidence.topic.locations.all()] or "-"

    @admin.display(description="Actors")
    def get_topic_actors(self, evidence: Evidence):
        return [actor.name for actor in evidence.topic.actors.all()] or "-"

    @admin.display(description="Impacts")
    def get_topic_impacts(self, evidence: Evidence):
        return [impact.name for impact in evidence.topic.impacts.all()] or "-"

    @admin.display(description="Activities")
    def get_topic_activities(self, evidence: Evidence):
        return [activity.name for activity in evidence.topic.activities.all()] or "-"

    @admin.display(description="Document")
    @admin.display(ordering="document__title")
    def document_title(self, evidence: Evidence):
        return evidence.document.title

    @admin.display(description="Release Date")
    @admin.display(ordering="document__release_date")
    def document_date(self, evidence: Evidence):
        return evidence.document.release_date


class TopicEvidenceInline(admin.TabularInline):
    model = Evidence
    show_change_link = True
    extra = 2
    fields = ['document', 'reference', 'relevance']


class TopicAdmin(admin.ModelAdmin):
    save_on_top = True
    #inlines = [TopicEvidenceInline]
    readonly_fields = ['id']
    fields = ['id', 'products', 'locations', 'actors', 'impacts', 'activities']
    list_display = ['id', 'get_products', 'get_locations', 'get_actors', 'get_impacts', 'get_activities']
    list_filter = [('products', admin.RelatedOnlyFieldListFilter),
                   ('locations', admin.RelatedOnlyFieldListFilter),
                   ('actors', admin.RelatedOnlyFieldListFilter),
                   ('impacts', admin.RelatedOnlyFieldListFilter),
                   ('activities', admin.RelatedOnlyFieldListFilter)]
    #filter_horizontal = ['products', 'locations', 'actors', 'impacts', 'activities']
    autocomplete_fields = ['products', 'locations', 'actors', 'impacts', 'activities']
    list_display_links = list_display
    list_select_related = True
    ordering = ['products', 'locations', 'actors', 'impacts', 'activities']

    @admin.display(description="Products")
    @admin.display(ordering="products")
    def get_products(self, topic: Topic):
        return [product.name for product in topic.products.all()] or "-"

    @admin.display(description="Locations")
    @admin.display(ordering="locations")
    def get_locations(self, topic: Topic):
        return [location.name for location in topic.locations.all()] or "-"

    @admin.display(description="Actors")
    @admin.display(ordering="actors")
    def get_actors(self, topic: Topic):
        return [actor.name for actor in topic.actors.all()] or "-"

    @admin.display(description="Impacts")
    @admin.display(ordering="impacts")
    def get_impacts(self, topic: Topic):
        return [impact.name for impact in topic.impacts.all()] or "-"

    @admin.display(description="Activities")
    @admin.display(ordering="activities")
    def get_activities(self, topic: Topic):
        return [activity.name for activity in topic.activities.all()] or "-"


# Register your models here.
documents_admin_site.register(Document, DocumentAdmin)
documents_admin_site.register(Actor, ActorAdmin)
documents_admin_site.register(ActorCategory)
documents_admin_site.register(Product, ProductAdmin)
documents_admin_site.register(ProductCategory)
documents_admin_site.register(Location, LocationAdmin)
documents_admin_site.register(LocationCategory)
documents_admin_site.register(Impact, ImpactAdmin)
documents_admin_site.register(Activity, ActivityAdmin)
documents_admin_site.register(Topic, TopicAdmin)
documents_admin_site.register(Evidence, EvidenceAdmin)
