from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *

Permission.objects.get(codename__exact='add_stakeholder').delete()
Permission.objects.get(codename__exact='change_stakeholder').delete()
Permission.objects.get(codename__exact='view_stakeholder').delete()
Permission.objects.get(codename__exact='delete_stakeholder').delete()
o = Permission.objects.get(codename__exact='add_actor')
o.name = "Can add actor"
o.save()
o = Permission.objects.get(codename__exact='change_actor')
o.name = "Can change actor"
o.save()
o = Permission.objects.get(codename__exact='view_actor')
o.name = "Can view actor"
o.save()
o = Permission.objects.get(codename__exact='delete_actor')
o.name = "Can delete actor"
o.save()
Permission.objects.get(codename__exact='add_stakeholdercategory').delete()
Permission.objects.get(codename__exact='change_stakeholdercategory').delete()
Permission.objects.get(codename__exact='view_stakeholdercategory').delete()
Permission.objects.get(codename__exact='delete_stakeholdercategory').delete()
Permission.objects.get(codename__exact='add_stakeholdercategoty').delete()
Permission.objects.get(codename__exact='change_stakeholdercategoty').delete()
Permission.objects.get(codename__exact='view_stakeholdercategoty').delete()
Permission.objects.get(codename__exact='delete_stakeholdercategoty').delete()
o = Permission.objects.get(codename__exact='add_actorcategory')
o.name = "Can add actor category"
o.save()
o = Permission.objects.get(codename__exact='change_actorcategory')
o.name = "Can change actor category"
o.save()
o = Permission.objects.get(codename__exact='view_actorcategory')
o.name = "Can view actor category"
o.save()
o = Permission.objects.get(codename__exact='delete_actorcategory')
o.name = "Can delete actor category"
o.save()
Permission.objects.get(codename__exact='add_action').delete()
Permission.objects.get(codename__exact='change_action').delete()
Permission.objects.get(codename__exact='view_action').delete()
Permission.objects.get(codename__exact='delete_action').delete()
o = Permission.objects.get(codename__exact='add_activity')
o.name = "Can add activity"
o.save()
o = Permission.objects.get(codename__exact='change_activity')
o.name = "Can change activity"
o.save()
o = Permission.objects.get(codename__exact='view_activity')
o.name = "Can view activity"
o.save()
o = Permission.objects.get(codename__exact='delete_activity')
o.name = "Can delete activity"
o.save()
o = Permission.objects.get(codename__exact='add_evidence')
o.name = "Can add evidence"
o.save()
o = Permission.objects.get(codename__exact='change_evidence')
o.name = "Can change evidence"
o.save()
o = Permission.objects.get(codename__exact='view_evidence')
o.name = "Can view evidence"
o.save()
o = Permission.objects.get(codename__exact='delete_evidence')
o.name = "Can delete evidence"
o.save()
o = Permission.objects.get(codename__exact='add_topic')
o.name = "Can add topic"
o.save()
o = Permission.objects.get(codename__exact='change_topic')
o.name = "Can change topic"
o.save()
o = Permission.objects.get(codename__exact='view_topic')
o.name = "Can view topic"
o.save()
o = Permission.objects.get(codename__exact='delete_topic')
o.name = "Can delete topic"
o.save()
topic = ContentType.objects.get(model__exact='topic')
Permission.objects.get(codename__exact='add_impact', content_type=topic).delete()
Permission.objects.get(codename__exact='change_impact', content_type=topic).delete()
Permission.objects.get(codename__exact='view_impact', content_type=topic).delete()
Permission.objects.get(codename__exact='delete_impact', content_type=topic).delete()
o = Permission.objects.get(codename__exact='add_impact')
o.name = "Can add impact"
o.save()
o = Permission.objects.get(codename__exact='change_impact')
o.name = "Can change impact"
o.save()
o = Permission.objects.get(codename__exact='view_impact')
o.name = "Can view impact"
o.save()
o = Permission.objects.get(codename__exact='delete_impact')
o.name = "Can delete impact"
o.save()
