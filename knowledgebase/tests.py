from unicodedata import category
from django.test import TestCase
from .models import Document, LocationCategory, Product, ProductCategory, Location, Topic, Evidence
from .filters import EvidenceFilter


class ActualDataTest(TestCase):

    # Konkrete Evidence mit 1 Document
    def test_single_full_evidence_query(self):
        # Given
        cl = LocationCategory.objects.create()
        cp = ProductCategory.objects.create()
        p = Product.objects.create(category=cp)
        l = Location.objects.create(category=cl)
        t = Topic.objects.create()
        t.products.add(p)
        t.locations.add(l)
        d = Document.objects.create()
        Evidence.objects.create(document=d, topic=t, relevance=2)
        # When
        efs = EvidenceFilter(data={"location": l.pk, "product": p.pk},
                             queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 1)
        self.assertEquals(result.get().document, d)
        self.assertEquals(result.get().relevance, 2)

    # Offene Evidence mit doppeltem Ergebnisdokument in korrekter Ordnung
    def test_double_open_evidence_single_document_query_in_order(self):
        # Given
        cl = LocationCategory.objects.create()
        cp = ProductCategory.objects.create()
        p = Product.objects.create(category=cp)
        l1 = Location.objects.create(category=cl)
        l2 = Location.objects.create(category=cl)
        t1 = Topic.objects.create()
        t1.products.add(p)
        t1.locations.add(l1)
        t2 = Topic.objects.create()
        t2.products.add(p)
        t2.locations.add(l2)
        d1 = Document.objects.create()
        d2 = Document.objects.create()
        Evidence.objects.create(document=d1, topic=t2, relevance=8)
        Evidence.objects.create(document=d1, topic=t1, relevance=2)
        # When
        efs = EvidenceFilter(data={"product": p.pk},
                                queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 2)
        self.assertEquals(result[0].document, d1)
        self.assertIs(result[0].relevance, 2)
        self.assertEquals(result[1].document, d1)
        self.assertIs(result[1].relevance, 8)

    # Offene Evidence mit 2 Ergebnisdokumenten in korrekter Ordnung
    def test_double_open_evidence_double_document_query_in_order(self):
        # Given
        cl = LocationCategory.objects.create()
        cp = ProductCategory.objects.create()
        p = Product.objects.create(category=cp)
        l = Location.objects.create(category=cl)
        t1 = Topic.objects.create()
        t1.products.add(p)
        t1.locations.add(l)
        t2 = Topic.objects.create()
        t2.products.add(p)
        d1 = Document.objects.create()
        d2 = Document.objects.create()
        Evidence.objects.create(document=d2, topic=t2, relevance=8)
        Evidence.objects.create(document=d1, topic=t1, relevance=5)
        # When
        efs = EvidenceFilter(data={"product": p.pk}, 
                                queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 2)
        self.assertEquals(result[0].document, d1)
        self.assertIs(result[0].relevance, 5)
        self.assertEquals(result[1].document, d2)
        self.assertIs(result[1].relevance, 8)

    # Angefragte Parent-Location bekommt auch Child-Location, wenn sie passt
    def test_parent_location_also_child_query(self):
        # Given
        cl = LocationCategory.objects.create()
        cp = ProductCategory.objects.create()
        p = Product.objects.create(category=cp)
        l1 = Location.objects.create(category=cl)
        l2 = Location.objects.create(category=cl)
        lp = Location.objects.create(category=cl)
        lp.members.add(l1)
        lp.members.add(l2)
        t = Topic.objects.create()
        t.products.add(p)
        t.locations.add(l1)
        d1 = Document.objects.create()
        d2 = Document.objects.create()
        Evidence.objects.create(document=d1, topic=t)
        # When
        efs = EvidenceFilter(data={"location": lp.pk}, 
                                queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 1)
        self.assertEquals(result.get().document, d1)

    # Angefragte Grand-Parent-Location bekommt auch Grand-Child-Location 
    def test_grand_parent_location_also_grand_child_query(self):
        # Given
        cl = LocationCategory.objects.create()
        cp = ProductCategory.objects.create()
        p = Product.objects.create(category=cp)
        l = Location.objects.create(category=cl)
        lp = Location.objects.create(category=cl)
        lp.members.add(l)
        lpp = Location.objects.create(category=cl)
        lpp.members.add(lp)
        t = Topic.objects.create()
        t.products.add(p)
        t.locations.add(l)
        d = Document.objects.create()
        Evidence.objects.create(document=d, topic=t)
        # When
        efs = EvidenceFilter(data={"location": lpp.pk}, 
                                queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 1)
        self.assertEquals(result.get().document, d)

    # Angefragte Child-Location bekommt keine Parent-Location
    def test_child_location_no_parent_query(self):
        # Given
        cl = LocationCategory.objects.create()
        l = Location.objects.create(category=cl)
        lp = Location.objects.create(category=cl)
        lp.members.add(l)
        t = Topic.objects.create()
        t.locations.add(lp)
        d = Document.objects.create()
        # When
        Evidence.objects.create(document=d, topic=t)
        efs = EvidenceFilter(data={"location": l.pk}, 
                                queryset=Evidence.objects.all())
        result = efs.qs
        # Then
        self.assertIs(result.count(), 0)
