from django.contrib.auth.models import User, Group
from rest_framework import serializers
from knowledgebase.models import Actor, Activity, Product, Location, Impact,\
                                 Document, Topic, Evidence,\
                                 ActorCategory, ProductCategory, LocationCategory


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['pk', 'url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['pk', 'url', 'name']


class ActorCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ActorCategory
        fields = ['pk', 'url', 'name', 'description']


class ActorSerializer(serializers.HyperlinkedModelSerializer):
    category = ActorCategorySerializer()

    class Meta:
        model = Actor
        fields = ['pk', 'url', 'name', 'category']


class ProductCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProductCategory
        fields = ['pk', 'url', 'name', 'description']


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    category = ProductCategorySerializer()

    class Meta:
        model = Product
        fields = ['pk', 'url', 'name', 'category']

    
class LocationCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LocationCategory
        fields = ['pk', 'url', 'name', 'description']


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    category = LocationCategorySerializer()

    class Meta:
        model = Location
        fields = ['pk', 'url', 'name', 'category']


class ImpactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Impact
        fields = ['pk', 'url', 'name']


class ActivitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Activity
        fields = ['pk', 'url', 'name']


class FlatEvidenceSerializer(serializers.HyperlinkedModelSerializer):
    document = serializers.ReadOnlyField(source='document.pk')
    topic = serializers.ReadOnlyField(source='topic.pk')

    class Meta:
        model = Evidence
        fields = ['pk', 'url', 'document', 'topic', 'relevance', 'reference']


class FlatTopicSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Topic
        fields = ['pk', 'url', 'products', 'locations', 'actors', 'impacts', 'activities']


class DeepTopicSerializer(serializers.HyperlinkedModelSerializer):
    products = ProductSerializer(many=True)
    locations = LocationSerializer(many=True)
    actors = ActorSerializer(many=True)
    impacts = ImpactSerializer(many=True)
    activities = ActivitySerializer(many=True)

    class Meta:
        model = Topic
        fields = ['pk', 'url', 'products', 'locations', 'actors', 'impacts', 'activities']


class DocumentSerializer(serializers.HyperlinkedModelSerializer):
    assocs = ActorSerializer(many=True)
    evidences = FlatEvidenceSerializer(many=True)

    class Meta:
        model = Document
        fields = ['pk', 'url', 'title', 'subtitle', 'assocs', 'release_date', 'source', 'file', 'abstract',
                  'volume', 'note', 'todo', 'evidences', 'created_at', 'updated_at']
        extra_kwargs = {'topics': {'required': False}}  # Do not display 'real' topics field


class DeepEvidenceSerializer(serializers.HyperlinkedModelSerializer):
    document = DocumentSerializer()
    topic = DeepTopicSerializer()

    class Meta:
        model = Evidence
        fields = ['pk', 'url', 'document', 'topic', 'relevance', 'reference']


class TopicEvidenceSerializer(serializers.HyperlinkedModelSerializer):
    topic = DeepTopicSerializer()

    class Meta:
        model = Evidence
        fields = ['pk', 'url', 'document', 'topic', 'relevance', 'reference']


class SimpleGroupedEvidenceSerializer(serializers.HyperlinkedModelSerializer):
    avg_relevance = serializers.ReadOnlyField()
    document = DocumentSerializer()

    class Meta:
        model = Evidence
        fields = ['avg_relevance', 'document']


class DeepGroupedEvidenceSerializer(serializers.HyperlinkedModelSerializer):
    avg_relevance = serializers.ReadOnlyField()
    document = DocumentSerializer()
    evidences = DeepEvidenceSerializer(many=True)

    class Meta:
        model = Evidence
        fields = ['avg_relevance', 'document', 'evidences']
