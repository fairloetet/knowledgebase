import django_filters
from django.db.models.query import QuerySet
from django_filters import rest_framework as rest_filters
from knowledgebase.models import Product, Location, Impact, Evidence, AbstractEntity, Document
from django.db.models import Aggregate, Avg, CharField, Value, Min
from operator import attrgetter


# Taken from https://stackoverflow.com/questions/10340684/
# See https://docs.djangoproject.com/en/4.2/ref/contrib/postgres/aggregates/ StringAgg for postgres
# See https://django-mysql.readthedocs.org/en/latest/aggregates.html#django_mysql.models.GroupConcat for MySQL
# TODO Avoid SQL injection: https://docs.djangoproject.com/en/4.2/ref/models/expressions/#avoiding-sql-injection
class Concat(Aggregate):
    # This is for SQLite and MySQL
    function = "GROUP_CONCAT"
    separator = ","  # See usages of Concat(), but for SQLite, it needs to be a comma

    def __init__(self, expression, distinct=False, ordering=None, **extra_context):
        super().__init__(
            expression,
            distinct="DISTINCT " if distinct else "",
            ordering=" ORDER BY %s" % ordering if ordering is not None else "",
            output_field=CharField(),
            **extra_context
        )

    # https://stackoverflow.com/a/77134474
    def as_postgresql(self, compiler, connection, separator=separator, **extra_context):
        return super().as_sql(
            compiler,
            connection,
            function="STRING_AGG",  # or ARRAY_AGG, but probably remove ::text then
            template="%(function)s(%(expressions)s::text,%(separator)s%(ordering)s)",
            separator="'%s'" % separator,
            **extra_context
        )

    def as_mysql(self, compiler, connection, separator=separator, **extra_context):
        return super().as_sql(
            compiler,
            connection,
            template="%(function)s(%(distinct)s%(expressions)s%(ordering)s%(separator)s)",
            separator=" SEPARATOR '%s'" % separator,
            **extra_context
        )

    def as_sqlite(self, compiler, connection, **extra_context):
        return super().as_sql(
            compiler,
            connection,
            template="%(function)s(%(distinct)s%(expressions)s%(ordering)s)",
            # No separator option in SQLite
            **extra_context
        )

# With selected AbstractEntity, get all evidence's topics with this entity and all the entities's members recursively below
# Based on https://stackoverflow.com/questions/65040416/using-django-mptt-in-django-filters-to-get-children-on-filtering-parent-node
# TODO This doesn't allow to annotate the found evidences with the level in the tree for better computation of relevance
# TODO   Or is there a way to annotate() a level value?
# TODO Tried to annotate individual levels this way, but annotating after union is not allowed. 
# TODO   The OR (|) operator doesn't seem to be correct, or?
#level_1_qs = queryset.annotate(level=Value(0)).filter(**{f"{field_name}__in": entity.get_community_level(0)})
#level_2_qs = queryset.annotate(level=Value(1)).filter(**{f"{field_name}__in": entity.get_community_level(1)})
#level_3_qs = queryset.annotate(level=Value(2)).filter(**{f"{field_name}__in": entity.get_community_level(2)})
#return level_1_qs.union(level_2_qs).union(level_3_qs)
def evidence_entity_with_community(queryset: QuerySet, field_name: str, entity: AbstractEntity):
    # For this tricky str-based way of filter definition see https://django-filter.readthedocs.io/en/latest/ref/filters.html#filter-method
    # It's equivalent to e.g. queryset.filter(topic__products__in=product.get_community())
    return queryset.filter(**{f"{field_name}__in": entity.get_community()})


# Ordering is normally done by starting with an ordered queryset, but the order is dependant on
#   the results, because it depends which evidences are selected, and in fact, Evidences are ordered
def order_evidence_queryset(queryset: QuerySet):
    return queryset.order_by('relevance', '-document__release_date')  # .distinct() doesn't work for SQLite


class EvidenceFilter(rest_filters.FilterSet, django_filters.FilterSet):

    product = django_filters.ModelChoiceFilter(
        queryset=Product.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="topic__products",
        label="Product",
        method=evidence_entity_with_community
        )
    location = django_filters.ModelChoiceFilter(
        queryset=Location.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="topic__locations",
        label="Location",
        method=evidence_entity_with_community
        )
    impact = django_filters.ModelChoiceFilter(
        queryset=Impact.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="topic__impacts",
        label="Impact",
        method=evidence_entity_with_community
        )
    # To have more control on the ordering, it is done in the filter_queryset below
    # See https://github.com/carltongibson/django-filter/issues/274 on the issue of having > 1 order fields
    #sort = django_filters.OrderingFilter(
    #    choices={
    #        ('relevance', 'Relevance'),
    #        ('release_date', 'Release Date'),
    #    },
    #    fields={
    #        'evidences': 'relevance',
    #        'release_date': 'release_date',
    #    },
    #)

    class Meta:
        model = Evidence
        fields = ['product', 'location', 'impact']
        # This will allow to use different lookups in case of free CharFilter, not ChoiceFilter
        # fields = {
        #     'product': ['exact', 'contains'],
        #     'location': ['exact', 'contains'],
        #     'impact': ['exact', 'contains']
        # }

    def is_valid(self):
        # Force to select at least one value, not to list all documents in one query
        if self.data == {} or all(self.data[v] == "" for v in self.data):
            return False
        return super().is_valid()
    

class SimpleGroupedEvidenceFilter(EvidenceFilter):

    @property
    def qs(self):
        queryset = super().qs
        groups = queryset.group_by('document').\
            annotate(avg_relevance=Avg('relevance')).\
            order_by("avg_relevance", "-document__release_date")
        return groups


class DeepGroupedEvidenceFilter(EvidenceFilter):

    @property
    def qs(self):
        queryset = super().qs
        groups = queryset.values('document').\
            annotate(avg_relevance=Avg('relevance'), evidence_pks=Concat('pk')).\
            order_by("avg_relevance", "-document__release_date")
        # TODO Dies holt nun aber auch alle Elemente. Nix mit lazy und paging :-(
        for group in groups:
            group["document"] = Document.objects.get(pk=group['document'])
            evidences = [Evidence.objects.get(pk=evidence_pk) for evidence_pk in group['evidence_pks'].split(',')]
            group["evidences"] = sorted(evidences, key=attrgetter("relevance"), reverse=True)
        return groups


class DocumentFilter(rest_filters.FilterSet, django_filters.FilterSet):

    product = django_filters.ModelChoiceFilter(
        queryset=Product.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="evidences__topic__products",
        label="Product",
        method=evidence_entity_with_community
        )
    location = django_filters.ModelChoiceFilter(
        queryset=Location.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="evidences__topic__locations",
        label="Location",
        method=evidence_entity_with_community
        )
    impact = django_filters.ModelChoiceFilter(
        queryset=Impact.objects.prefetch_related('members').all(),
        #distinct=True,
        field_name="evidences__topic__impacts",
        label="Impact",
        method=evidence_entity_with_community
        )

    class Meta:
        model = Document
        fields = ['product', 'location', 'impact']

    @property
    def qs(self):
        return super().qs.distinct().order_by('-release_date')
    
    #def filter_queryset(self, queryset):
    #    queryset = super().filter_queryset(queryset)
    #    return order_evidence_queryset(queryset)

    def is_valid(self):
        # Force to select at least one value, not to list all documents in one go
        if self.data == {}:  # or all(self.data[v] == "" for v in self.data)
            return False
        return super().is_valid()



