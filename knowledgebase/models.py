from textwrap import shorten
from django.db import models
from django.utils.text import slugify
from django.core.validators import MaxValueValidator, MinValueValidator, FileExtensionValidator
from django_group_by import GroupByMixin
from django.db.models.functions import Lower
from django.core.files.storage import storages
# from s3_file_field import S3FileField

# --------------------------------


class NoUpperCaseCharField(models.CharField):

    def __init__(self, *args, **kwargs):
        super(NoUpperCaseCharField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if value and str(value).isupper():
            # value = capwords(str(value))
            # Based on https://stackoverflow.com/a/42500863
            value = ' '.join((w[:1].upper() + w[1:] if w not in ['for', 'and', 'in', 'against', 'to', 'on', 'the', 'of', 'für', 'und', 'der', 'die', 'das', 'über', 'von'] else w) 
                             for w in str(value).lower().split(' '))
            value = value[0].upper + value[1:]
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(NoUpperCaseCharField, self).pre_save(model_instance, add)

# --------------------------------


class AbstractCategory(models.Model):
    name = models.CharField(blank=False, unique=True, max_length=50)
    alternative_names = models.CharField(blank=True, max_length=250)
    description = models.TextField(blank=True)

    class Meta:
        abstract = True
        ordering = [Lower('name')]

    def __str__(self) -> str:
        return f"{self.name}"


# TODO Alle Versuche, in diese Oberklasse auch noch die Category (und zusammen mit name auch __str__) reinzunehmen
# sind bislang gescheitert. Die name könnten zudem verschieden lang sein bei den verschiedenen Entities
class AbstractEntity(models.Model):
    members = models.ManyToManyField('self', symmetrical=False, blank=True, related_name="memberships")
    # level: int

    class Meta:
        abstract = True

    # Returns members and it's sub-members alike, including self. Inspired by https://stackoverflow.com/questions/4725343/
    # Sure this becomes quite slow in case of deep hierarchies. TODO Some prefetch or other Django tricks may help here
    # TODO This is depth first search. Breadth first could enable a more relevant ordering of filter results.
    #   Having this, a level should be set for each member entity, see comments
    # It used to use https://pypi.org/project/django-tree-queries/ which TreeNode has a parent and allows to call
    #   descendants(include_self=True) on all nodes to perform the same. But in in queries we need the children only.
    # Other implementation like django-tree-queries, django-mptt (not supported anymore) or django-treebeard may support
    #   a more efficient implementation, but - apart from django-tree-queries - require much more effort in using it
    def get_community(self):  #(self, level: int = 0):
        #self.level = level
        result = {self}
        for member in self.members.all():
            result |= member.get_community()  # (level+1)
        return result

    def get_near_community(self, level: int):
        result = {self}
        if level > 0:
            for member in self.members.all():
                result |= member.get_near_community(level-1)
        return result

    def get_community_level(self, level: int):
        if level > 0:
            if level == 1:
                result = {member for member in self.members.all()}
            else:
                result = set()
                for member in self.members.all():
                    result |= member.get_community_level(level-1)
        else:
            result = {self}
        return result

    def has_members(self) -> bool:
        return self.members.count() > 0

    def is_member(self) -> bool:
        return self.memberships.count() > 0

    def get_members(self):
        # TODO Beware, this issues an SQL call for each row in the table
        return [member for member in self.members.all()]

    def get_memberships(self):
        # TODO Beware, this issues an SQL call for each row in the table
        return [membership for membership in self.memberships.all()]

    # Used for prettier display in admin
    has_members.boolean = True
    is_member.boolean = True
    get_members.short_description = "Members"
    get_memberships.short_description = "Memberships"


class ActorCategory(AbstractCategory):
    class Meta(AbstractCategory.Meta):
        verbose_name = "Actor Category"
        verbose_name_plural = "Actor Categories"


class Actor(AbstractEntity):
    name = models.CharField(blank=False, max_length=100)
    abbreviation = models.CharField(blank=True, max_length=20)
    alternative_names = models.CharField(blank=True, max_length=250)
    category = models.ForeignKey(ActorCategory, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = [Lower('name')]
        verbose_name = "Actor"
        verbose_name_plural = "Actors"

    def __str__(self) -> str:
        return f"{self.abbreviation if self.abbreviation else self.name}"


class ProductCategory(AbstractCategory):
    class Meta(AbstractCategory.Meta):
        verbose_name = "Product Category"
        verbose_name_plural = "Product Categories"


class Product(AbstractEntity):
    name = models.CharField(blank=False, max_length=100)
    alternative_names = models.CharField(blank=True, max_length=250)
    category = models.ForeignKey(ProductCategory, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = [Lower('name')]
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self) -> str:
        return f"{self.name} ({self.category})"


class LocationCategory(AbstractCategory):
    class Meta(AbstractCategory.Meta):
        verbose_name = "Location Category"
        verbose_name_plural = "Location Categories"


class Location(AbstractEntity):
    name = models.CharField(blank=False, max_length=100)
    abbreviation = models.CharField(blank=True, max_length=20)
    alternative_names = models.CharField(blank=True, max_length=250)
    category = models.ForeignKey(LocationCategory, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = [Lower('name')]
        verbose_name = "Location"
        verbose_name_plural = "Locations"

    def __str__(self) -> str:
        return f"{self.name} ({self.category})"


class Impact(AbstractEntity):
    name = models.CharField(blank=False, max_length=50)
    alternative_names = models.CharField(blank=True, max_length=250)

    class Meta:
        ordering = [Lower('name')]
        verbose_name = "Impact"
        verbose_name_plural = "Impacts"

    def __str__(self) -> str:
        return f"{self.name}"


class Activity(AbstractEntity):
    name = models.CharField(blank=False, max_length=50)
    alternative_names = models.CharField(blank=True, max_length=250)

    class Meta:
        ordering = [Lower('name')]
        verbose_name = "Activity"
        verbose_name_plural = "Activities"

    def __str__(self) -> str:
        return f"{self.name}"


class Topic(models.Model):
    products = models.ManyToManyField(Product, blank=True, related_name="topics")
    locations = models.ManyToManyField(Location, blank=True, related_name="topics")
    actors = models.ManyToManyField(Actor, blank=True, related_name="topics")
    impacts = models.ManyToManyField(Impact, blank=True, related_name="topics")
    activities = models.ManyToManyField(Activity, blank=True, related_name="topics")

    class Meta:
        verbose_name = "Topic"
        verbose_name_plural = "Topics"

    # This is complicated to query, use it with caution, e.g. don't fill dropdown boxes using __str__.
    def __str__(self) -> str:
        return f"{shorten(', '.join([p.name for p in self.products.all()]), width=50, placeholder='...') or '-'} / " \
               f"{shorten(', '.join([l.name for l in self.locations.all()]), width=50, placeholder='...') or '-'} / " \
               f"{shorten(', '.join([str(a) for a in self.actors.all()]), width=50, placeholder='...') or '-'} / " \
               f"{shorten(', '.join([i.name for i in self.impacts.all()]), width=50, placeholder='...') or '-'} / " \
               f"{shorten(', '.join([a.name for a in self.activities.all()]), width=50, placeholder='...') or '-'}"


def upload_file(instance, filename):
    file_path = "documents/{0}/{1}/{2}.{3}".format(
        instance.release_date.year if instance.release_date else "",
        instance.release_date.month if (instance.release_date and
                                        (instance.release_date.day != 1 or instance.release_date.month != 1)) else "",
        slugify(instance.title), "pdf")
    return file_path


class Document(models.Model):
    title = NoUpperCaseCharField(blank=False, max_length=250)
    subtitle = NoUpperCaseCharField(max_length=250, blank=True)
    author = NoUpperCaseCharField(max_length=250, blank=True, verbose_name="Author(s)")
    assocs = models.ManyToManyField(Actor, blank=True, verbose_name="Association(s)")
    release_date = models.DateField(blank=True, null=True)
    volume = NoUpperCaseCharField(max_length=250, blank=True, help_text="Enclosing document or series")
    source = models.URLField(blank=True, null=True)
    abstract = models.TextField(blank=True)
    file = models.FileField(blank=True, null=True, storage=storages["mediafiles"],
                            upload_to=upload_file, validators=[FileExtensionValidator(["pdf"])])
    #file = S3FileField(blank=True, null=True, storage=storages["mediafiles"],
    #                    upload_to=upload_file, validators=[FileExtensionValidator(["pdf"])])
    note = models.TextField(blank=True, help_text="Just some notes after reading the document")
    todo = models.CharField(max_length=250, blank=True, help_text="Leftovers on tagging or reading this document")
    topics = models.ManyToManyField(Topic, blank=True, through='Evidence', related_name="documents")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["release_date"]
        verbose_name = "Document"
        verbose_name_plural = "Documents"

    # This is quite complicated to query, use it with caution.
    def __str__(self) -> str:
        assoc_list = self.get_assocs()
        return f"\"{self.title}\" by {', '.join([str(a) for a in assoc_list]) if len(assoc_list) > 0 else self.author}"

    def get_assocs(self):
        # TODO Beware, this issues an SQL call for each row in the table
        return [assoc for assoc in self.assocs.all()]

    get_assocs.short_description = "Associations"


class EvidenceQuerySet(models.QuerySet, GroupByMixin):
    pass


class Relevance(models.IntegerChoices):
    High = 2
    Useful = 5
    Weak = 8


class Evidence(models.Model):
    document = models.ForeignKey(Document, on_delete=models.DO_NOTHING, related_name='evidences')
    topic = models.ForeignKey(Topic, on_delete=models.DO_NOTHING, related_name='evidences')
    reference = models.CharField(max_length=20, blank=True,
                                 help_text="Section, chapter, page or similar reference within the document")
    relevance = models.IntegerField(choices=Relevance.choices)

    objects = EvidenceQuerySet.as_manager()

    class Meta:
        ordering = ['relevance', '-document__release_date']
        verbose_name = "Evidence"
        verbose_name_plural = "Evidences"  # Generally, there's no plural, but in this context...

    def __str__(self) -> str:
        return f"{self.topic} in \"{self.document.title}\""
