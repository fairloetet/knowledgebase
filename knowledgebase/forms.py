from django import forms
from knowledgebase.models import Document, Location, Product


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ("title", "subtitle", "abstract", "note" , "assocs")


class DocumentSearchForm(forms.Form):
    product = forms.ModelChoiceField(
        queryset=Product.objects.all()
        )
    product = forms.ModelChoiceField(
        queryset=Product.objects.all()
        )
