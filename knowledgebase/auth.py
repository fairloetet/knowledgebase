# Custom API Key handling, see https://stackoverflow.com/questions/28274825/key-based-access-for-the-django-rest-framework
from rest_framework.authentication import TokenAuthentication
#from rest_framework.request import Request

class TokenAuthGet(TokenAuthentication):
    """
    Extends the class to support token as "api_key" in a GET Query Parameter.
    Supports standard method in header as a default.
    """
    def authenticate(self, request):
        token = request.query_params.get("api_key", False)

        if token and "HTTP_AUTHORIZATION" not in request.META:
            return self.authenticate_credentials(token)
        else:
            return super().authenticate(request)
        

