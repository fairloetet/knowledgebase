# Generated by Django 4.2 on 2023-06-23 16:03

from django.db import migrations, models
import knowledgebase.models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0095_topic_activities_topic_actors_topic_impacts'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='topic',
            options={'verbose_name': 'Topic', 'verbose_name_plural': 'Topics'},
        ),
        migrations.RemoveField(
            model_name='topic',
            name='activity',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='actor',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='impact',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='location',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='product',
        ),
        migrations.AlterField(
            model_name='document',
            name='assocs',
            field=models.ManyToManyField(blank=True, to='knowledgebase.actor', verbose_name='association'),
        ),
        migrations.AlterField(
            model_name='document',
            name='author',
            field=knowledgebase.models.NoUpperCaseCharField(blank=True, max_length=250),
        ),
    ]
