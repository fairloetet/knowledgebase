# Generated by Django 4.2 on 2023-04-17 19:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0012_rename_stakeholdercategoty_stakeholdercategory_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='document',
            options={'ordering': ['release_date']},
        ),
        migrations.AlterField(
            model_name='impactcategory',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='knowledgebase.impactcategory'),
        ),
        migrations.AlterField(
            model_name='locationcategory',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='knowledgebase.locationcategory'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='knowledgebase.productcategory'),
        ),
        migrations.AlterField(
            model_name='stakeholdercategory',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='knowledgebase.stakeholdercategory'),
        ),
    ]
