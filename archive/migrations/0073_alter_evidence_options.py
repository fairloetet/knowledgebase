# Generated by Django 4.2 on 2023-05-11 17:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0072_alter_evidence_document_alter_evidence_topic'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='evidence',
            options={'ordering': ['relevance'], 'verbose_name': 'Evidence', 'verbose_name_plural': 'Evidences'},
        ),
    ]
