# Generated by Django 4.2 on 2023-05-11 17:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0070_alter_evidence_relevance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evidence',
            name='document',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='evidence', to='knowledgebase.document'),
        ),
        migrations.AlterField(
            model_name='evidence',
            name='topic',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='evidence', to='knowledgebase.topic'),
        ),
    ]
