# Generated by Django 4.2 on 2023-07-14 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0106_topic_a2'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='topic',
            name='a2',
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('actors', models.ManyToManyField(blank=True, related_name='test', to='knowledgebase.actor')),
            ],
        ),
    ]
