# Generated by Django 4.2 on 2023-05-11 08:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0065_alter_document_subtitle_alter_document_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='knowledgebase.location', verbose_name='parent'),
        ),
    ]
