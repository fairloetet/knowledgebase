# Generated by Django 4.2 on 2023-08-12 04:34

from django.db import migrations, models
import django.db.models.functions.text


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0114_remove_product_members2'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='activity',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Activity', 'verbose_name_plural': 'Activities'},
        ),
        migrations.AlterModelOptions(
            name='actor',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Actor', 'verbose_name_plural': 'Actors'},
        ),
        migrations.AlterModelOptions(
            name='actorcategory',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Actor Category', 'verbose_name_plural': 'Actor Categories'},
        ),
        migrations.AlterModelOptions(
            name='evidence',
            options={'ordering': ['relevance', '-document__release_date'], 'verbose_name': 'Evidence', 'verbose_name_plural': 'Evidences'},
        ),
        migrations.AlterModelOptions(
            name='impact',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Impact', 'verbose_name_plural': 'Impacts'},
        ),
        migrations.AlterModelOptions(
            name='location',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Location', 'verbose_name_plural': 'Locations'},
        ),
        migrations.AlterModelOptions(
            name='locationcategory',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Location Category', 'verbose_name_plural': 'Location Categories'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Product', 'verbose_name_plural': 'Products'},
        ),
        migrations.AlterModelOptions(
            name='productcategory',
            options={'ordering': [django.db.models.functions.text.Lower('name')], 'verbose_name': 'Product Category', 'verbose_name_plural': 'Product Categories'},
        ),
        migrations.AddField(
            model_name='activity',
            name='alternative_names',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
