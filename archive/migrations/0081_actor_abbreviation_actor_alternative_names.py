# Generated by Django 4.2 on 2023-05-14 04:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0080_activity_parent_alter_document_assocs'),
    ]

    operations = [
        migrations.AddField(
            model_name='actor',
            name='abbreviation',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='actor',
            name='alternative_names',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
