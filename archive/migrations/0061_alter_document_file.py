# Generated by Django 4.2 on 2023-05-04 12:30

import django.core.validators
from django.db import migrations, models
import knowledgebase.models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0060_remove_actorcategory_parent_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to=knowledgebase.models.upload_file, validators=[django.core.validators.FileExtensionValidator(['pdf'])]),
        ),
    ]
