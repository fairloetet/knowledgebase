# Generated by Django 4.2 on 2023-06-01 15:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0089_alter_product_tags_alter_product_tree_tags_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='tree_tags',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='product_tree_tags',
        ),
        migrations.DeleteModel(
            name='ProductTreeTag',
        ),
    ]
