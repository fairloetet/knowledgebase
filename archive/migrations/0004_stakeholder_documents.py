# Generated by Django 4.2 on 2023-04-14 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0003_document_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='stakeholder',
            name='documents',
            field=models.ManyToManyField(to='knowledgebase.document'),
        ),
    ]
