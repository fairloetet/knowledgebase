# Generated by Django 4.2 on 2023-06-07 19:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('knowledgebase', '0091_remove_product_tags_remove_topic_product_tags_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='topic',
            options={'ordering': ['product', 'location', 'actor', 'impact', 'activity'], 'verbose_name': 'Topic', 'verbose_name_plural': 'Topics'},
        ),
        migrations.AddField(
            model_name='topic',
            name='products',
            field=models.ManyToManyField(blank=True, related_name='products', to='knowledgebase.product'),
        ),
        migrations.AlterField(
            model_name='topic',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='product', to='knowledgebase.product'),
        ),
    ]
