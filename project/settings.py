"""
Django settings for the project.
Some data is defined in environment variables. The default values are for development purposes and must not be used in public.
"""

from pathlib import Path
import os

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

FIXTURE_DIRS = os.environ.get("DJANGO_FIXTURE_DIRS", default=os.path.join(BASE_DIR, '/archive/backups')).split(",")

DEBUG = bool(os.environ.get("DJANGO_DEBUG", default='True') == "True")

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY", 'django-insecure-@g1k^jx8ot5x&b!^()40(-#yts39@3g#axxctenbgq@(mp6fw0')


# Web security related
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", default="127.0.0.1").split(",")
INTERNAL_IPS = os.environ.get("INTERNAL_IPS", default="127.0.0.1").split(",")
CSRF_TRUSTED_ORIGINS = os.environ.get("CSRF_TRUSTED_ORIGINS", default="http://127.0.0.1:8000").split(",")
CSRF_COOKIE_SECURE = bool(os.environ.get("CSRF_COOKIE_SECURE", default='False') == "True")  # No SST in testing
SESSION_COOKIE_SECURE = bool(os.environ.get("SESSION_COOKIE_SECURE", default='False') == "True")  # No SST in testing
# HSTS is the policy to tell browsers to prefer https for this site in the future for all upcoming requests, for not redirecting every single call
SECURE_HSTS_SECONDS = int(os.environ.get("SECURE_HSTS_SECONDS", default="30"))  # Unit is seconds; use a small value (e.g. 30) for testing, huge value (e.g. 1 year = 31536000) in production
SECURE_HSTS_PRELOAD = bool(os.environ.get("SECURE_HSTS_PRELOAD", default='False') == "True")  # No SST in testing
SECURE_HSTS_INCLUDE_SUBDOMAINS = bool(os.environ.get("SECURE_HSTS_INCLUDE_SUBDOMAINS", default='False') == "True")  # No SST in testing
# The web server actually sends plain HTTP requests to Gunicorn/Django, but Django looks is the given header has the given value. Format: "header:value"
SECURE_PROXY_SSL_HEADER = os.environ.get("SECURE_PROXY_SSL_HEADER", default="HTTP_X_FORWARDED_PROTO:http").split(":")
# Note: We on purpose don't set SECURE_SSL_REDIRECT = True not to disallow HTTP. The web (proxy) server is expected to redirect http to https.
#   Unfortunately this gives an security.W008 warning, so we suppress it.
SILENCED_SYSTEM_CHECKS = ["security.W008"]
# How to fill the Refferer header on clicking links (besides leaving it empty when downgrading from https to http)
#   'strict-origin-when-cross-origin' instructs the browser to send the full URL when the link is same-origin and send only the server name when the link is cross-origin
SECURE_REFERRER_POLICY = "strict-origin-when-cross-origin"
X_FRAME_OPTIONS = 'SAMEORIGIN'
# Content-Security-Policy: See https://realpython.com/django-nginx-gunicorn/#adding-a-content-security-policy-csp-header
# Middleware https://django-csp.readthedocs.io is enabled and all values set to self
# Probabaly allow browsers to load other sources than from this server (=self), e.g. normalize.css from cdn.jsdelivr.net
# CSP_STYLE_SRC = ["'self'", "cdn.jsdelivr.net"]
# CSP_MEDIA_SRC = ["'self'", "nc.faire-elektronik.de"]
CSP_FRAME_ANCESTORS = ["'self'", "www.faire-elektronik.de", "faire-elektronik.de", "fairloetet.de"]  # To allow iframe embedding


# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": os.environ.get("DB_ENGINE", default='django.db.backends.sqlite3'),
        "NAME": os.environ.get("DB_NAME", default=os.path.join(BASE_DIR, 'db.sqlite3')),
        "USER": os.environ.get("DB_USER", default=""),
        "PASSWORD": os.environ.get("DB_PASSWORD", default=""),
        "HOST": os.environ.get("DB_HOST", default=""),
        "PORT": os.environ.get("DB_PORT", default=""),
    }
}

# Logging
# https://docs.djangoproject.com/en/4.2/topics/logging/#configuring-logging

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "INFO",
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": os.environ.get("DJANGO_LOG_LEVEL", default="INFO"),
            "propagate": False,
        },
    },}


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_api_key',
    'django_filters',
    'django_cleanup.apps.CleanupConfig',
    'minio_storage',
    'dbbackup',
#    's3_file_field',
    'knowledgebase',
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django_permissions_policy.PermissionsPolicyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
#    "kolo.middleware.KoloMiddleware",
    "csp.middleware.CSPMiddleware"
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/
LANGUAGE_CODE = 'en-gb'
TIME_ZONE = 'Europe/Berlin'
USE_I18N = True
USE_TZ = True


# Default Storages
# https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-STORAGES
STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
    },
    "mediafiles": {
        "BACKEND": os.environ.get("MEDIA_STORAGE", default="django.core.files.storage.FileSystemStorage")
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/
STATIC_URL = 'static/'
STATIC_ROOT = BASE_DIR / 'staticfiles/'
# STATICFILES_DIRS = BASE_DIR / 'static/'

# Media files (user uploaded content) location if storage "default" is FileSystemStorage
# https://docs.djangoproject.com/en/5.0/topics/files/
# Base url to serve media files
MEDIA_URL = '/media/'
# Path where media is stored
MEDIA_ROOT = BASE_DIR / 'mediafiles/'

# URL and credentials if a storage is WebDavStorage
WEBDAV_URL = os.environ.get("WEBDAV_URL", default='')   # Internal write/read access
WEBDAV_PUBLIC_URL = os.environ.get("WEBDAV_PUBLIC_URL", default=WEBDAV_URL)  # Public read access
WEBDAV_RECURSIVE_MKCOL = bool(os.environ.get("WEBDAV_RECURSIVE_MKCOL", default='True') == "True")

# URL and credentials if a storage is S3
AWS_ACCESS_KEY_ID = os.environ.get("S3_ACCESS_KEY", default="***")
AWS_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_KEY", default="***")
AWS_STORAGE_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME", default="knowledgebase")
AWS_S3_ENDPOINT_URL = os.environ.get("S3_ENDPOINT_URL", default="http://localhost:9000")  # For write access
# AWS_DEFAULT_ACL = 'public-read' # Do not store with private access
S3_PUBLIC_ACCESS_DOMAIN = os.environ.get("S3_PUBLIC_ACCESS_DOMAIN", default=None)  # For read access

# URL and credentials if a storage is MinIO
MINIO_STORAGE_ENDPOINT = os.environ.get("MINIO_STORAGE_ENDPOINT", default="localhost:9000")
MINIO_STORAGE_ACCESS_KEY = os.environ.get("MINIO_STORAGE_ACCESS_KEY", default="**")
MINIO_STORAGE_SECRET_KEY = os.environ.get("MINIO_STORAGE_SECRET_KEY", default="**")
MINIO_STORAGE_USE_HTTPS = bool(os.environ.get("MINIO_STORAGE_USE_HTTPS", default=False) == "True")
MINIO_STORAGE_MEDIA_BUCKET_NAME = os.environ.get("MINIO_STORAGE_MEDIA_BUCKET_NAME", default="knowledgebase")
MINIO_STORAGE_MEDIA_URL = os.environ.get("MINIO_STORAGE_MEDIA_URL", default=None)

# DB-Backup Storage
DBBACKUP_STORAGE = os.environ.get("BACKUP_STORAGE", default="django.core.files.storage.FileSystemStorage")
DBBACKUP_STORAGE_OPTIONS = os.environ.get("BACKUP_STORAGE_OPTIONS", default={'location': 'archive/backups/dbbackup/'})

# Permissions-Policy Header that allows a site to control which features and APIs can be used in the browser
# Seehttps://github.com/adamchainz/django-permissions-policy and https://developer.mozilla.org/en-US/docs/Web/HTTP/Permissions_Policy
# This is the most restrictive setting since we don't need anything
PERMISSIONS_POLICY = {
    "accelerometer": [],
    "ambient-light-sensor": [],
    "autoplay": [],
    "camera": [],
    "display-capture": [],
    "document-domain": [],
    "encrypted-media": [],
    "fullscreen": [],
    "geolocation": [],
    "gyroscope": [],
    "interest-cohort": [],
    "magnetometer": [],
    "microphone": [],
    "midi": [],
    "payment": [],
    "usb": [],
}


# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


# RESTful API
# https://www.django-rest-framework.org/ and https://www.django-rest-framework.org/tutorial/quickstart/
REST_FRAMEWORK = {
    # Note that this is the default. The views may define individual permission policies.
    'DEFAULT_PERMISSION_CLASSES': [
        # Use Django's standard `django.contrib.auth` permissions, but allow read-only access for unauthenticated users
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}
if not DEBUG:
    REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"].append( 
        # And require an API Key as well (i.e. add 'Authorization: Api-Key <API_KEY>' to the request's header), see
        #   https://florimondmanca.github.io/djangorestframework-api-key/guide/#making-authorized-requests
        'rest_framework_api_key.permissions.HasAPIKey'
        # 'knowledgebase.auth.TokenAuthGet'  # Doesn't work, see auth.py
    )

# Debug Toolbar config
# if DEBUG:
#   INSTALLED_APPS += [
#       'debug_toolbar'
#   ]
#
# if DEBUG:
#   MIDDLEWARE += [
#       'debug_toolbar.middleware.DebugToolbarMiddleware', # it must come after any other middleware that encodes the response’s content
#   ]
#
# if DEBUG:
#   DEBUG_TOOLBAR_PANELS = [
#       'debug_toolbar.panels.history.HistoryPanel',
#       'debug_toolbar.panels.versions.VersionsPanel',
#       'debug_toolbar.panels.timer.TimerPanel',
#       'debug_toolbar.panels.settings.SettingsPanel',
#       'debug_toolbar.panels.headers.HeadersPanel',
#       'debug_toolbar.panels.request.RequestPanel',
#       'debug_toolbar.panels.sql.SQLPanel',
#       'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#       'debug_toolbar.panels.templates.TemplatesPanel',
#       'debug_toolbar.panels.cache.CachePanel',
#       'debug_toolbar.panels.signals.SignalsPanel',
#       'debug_toolbar.panels.redirects.RedirectsPanel',
#       'debug_toolbar.panels.profiling.ProfilingPanel',
#   ]
#
# if DEBUG:
#   DISABLE_PANELS = {
#       'debug_toolbar.panels.history.HistoryPanel',
#       'debug_toolbar.panels.versions.VersionsPanel',
#       'debug_toolbar.panels.timer.TimerPanel',
#       'debug_toolbar.panels.settings.SettingsPanel',
#       'debug_toolbar.panels.headers.HeadersPanel',
#       'debug_toolbar.panels.request.RequestPanel',
#       'debug_toolbar.panels.sql.SQLPanel',
#       'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#       'debug_toolbar.panels.templates.TemplatesPanel',
#       'debug_toolbar.panels.cache.CachePanel',
#       'debug_toolbar.panels.signals.SignalsPanel',
#       'debug_toolbar.panels.redirects.RedirectsPanel',
#       'debug_toolbar.panels.profiling.ProfilingPanel',
# }
