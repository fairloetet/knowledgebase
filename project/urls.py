"""
URL configuration for the project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from knowledgebase.admin import documents_admin_site
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include("knowledgebase.urls")),
    path('doc/', include('django.contrib.admindocs.urls')),
    path('auth/', admin.site.urls),
    path("data/", documents_admin_site.urls),
    path('api/', include('rest_framework.urls', namespace='rest_framework')),
    path('s3-upload/', include('s3_file_field.urls')),
]
# Allow media files in development environment as well
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

if 'debug_toolbar' in settings.INSTALLED_APPS:
    urlpatterns += [path('__debug__/', include('debug_toolbar.urls'))]
