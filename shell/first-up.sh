docker compose -f docker-compose.prod.yml up -d --pull always
docker compose -f docker-compose.prod.yml exec web python manage.py diffsettings
docker compose -f docker-compose.prod.yml exec web python manage.py wait_for_db
docker compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input
docker compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
docker compose -f docker-compose.prod.yml exec web python manage.py check --database default --deploy
