# Inspired by https://medium.com/@albertazzir/blazing-fast-python-docker-builds-with-poetry-a78a66f5aed0


### 1. stage: Build virtual env for app
FROM python:3.10-bookworm as builder 

## Use poetry instead of pip
RUN pip install poetry==1.3.2

## Set environment variables for python and poetry
ENV PYTHONDONTWRITEBYTECODE=1 \ 
    PYTHONUNBUFFERED=1 \ 
    PIP_DISABLE_PIP_VERSION_CHECK=1 \ 
    POETRY_NO_INTERACTION=true \ 
    POETRY_VIRTUALENVS_CREATE=true \
    POETRY_VIRTUALENVS_IN_PROJECT=true

## Install python dependencies
# Copy poetry files. For some reason, poetry insists on having a readme
COPY pyproject.toml poetry.lock README.md ./
## Install only dependencies, creates virtual env in /.venv, and removes useless cache and artifacts
RUN poetry install --without dev --no-root && \
    rm -rf ~/.cache/pypoetry/{cache,artifacts}


### 2. stage: Pull slim image only for running, copying the previously virtual env
FROM python:3.10-slim-bookworm as runtime 

## For backup and insight, install psql
RUN apt update && apt -y install postgresql-client-15

## Create app/working directory
WORKDIR /app

## Retrieve virtual env from builder and activate it (i.e. setting VIRTUAL_ENV and extending PATH)
ENV VIRTUAL_ENV=/app/.venv \
    PATH="/app/.venv/bin:$PATH"
COPY --from=builder .venv .venv

## Copy knowldgebase project files and folders, avoids maintaining .dockerignore, but adds docker layers 
COPY manage.py ./
COPY project ./project
COPY knowledgebase ./knowledgebase

# Only needed if having scripts
# RUN pip install poetry && poetry install --without dev && pip uninstall poetry
